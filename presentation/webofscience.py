#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl
mpl.use('pgf')
import matplotlib.pyplot as plt

mpl.rcParams['pgf.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{unicode-math}',
    r'\usepackage[per-mode=reciprocal]{siunitx}'
]
mpl.rcParams['pgf.rcfonts'] = False
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.size'] = 20

years = np.arange(1970, 2015, 5)
values = np.array([151, 345, 683, 1379, 8934, 16902, 23294, 42751, 51387])

fig, ax = plt.subplots(1, 1)

ax.set_xlabel('Vuosi')
ax.set_ylabel('Julkaisut')

ax.bar(years, values, width=5)

fig.savefig('build/graphic/webofscience.pdf', bbox_inches='tight')
