SHELL := /bin/bash
MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDEXPANSION:

TEX_INPUTS := $(shell pwd):$(shell pwd)/header/:$(shell pwd)/header-components/
BIB_INPUTS := $(shell pwd)/

TEXENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(TEX_INPUTS):" max_print_line=1048576

build/main.pdf: main.tex header/bachelor.tex header/bachelor-packages.tex | build
	@tex-utils/tex.sh --tex-inputs "$(TEX_INPUTS)" --biber --bib-inputs "$(BIB_INPUTS)" main.tex

fast:
	@tex-utils/tex.sh --tex-inputs "$(TEX_INPUTS)" --biber --bib-inputs "$(BIB_INPUTS)" --fast main.tex

build:
	mkdir -p build

clean:
	rm -rf build

.PHONY: clean fast

GRAPHICS := algo.tikz Cottrell2007.pdf Morris1994-1.pdf Morris1994-2.pdf Ogitsu2003.pdf Huang2002-1.pdf Huang2002-2.pdf Backman2012.pdf

GRAPHICS := $(addprefix graphic/, $(GRAPHICS))

build/main.pdf: lit.bib $(GRAPHICS)
