This is my bachelor's thesis.
It is written in Finnish as required by the University of Helsinki.

> Molekyylidynaaminen menetelmä tasapainosimulaatioissa
>
> Risto Toijala, 2016
>
> University of Helsinki
>
> Department of Physics

The final thesis is saved as `thesis.pdf`.
The presentation given is saved as `presentation/presentation.pdf` with the slides on the left and notes on the right.
A summary of the presentation is saved as `presentation/summary.pdf`.
